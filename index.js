const connections = require("./config/db");

const grpc = require("grpc");

const chargectrl = require("./controllers/charge");

const PRORTO_PATH = `${__dirname}/proto/charge.proto`;

const EyowoCharge = grpc.load(PRORTO_PATH).EyowoCharge;

const server = new grpc.Server();

connections();

server.addService(EyowoCharge.Charge.service, chargectrl);

server.bind("0.0.0.0:50053", grpc.ServerCredentials.createInsecure());
server.start();

console.log({ status: "RUNNING....", service: "Eyowo Charges Service" });
