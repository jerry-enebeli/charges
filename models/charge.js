const mongoose = require("mongoose");

const { Schema } = mongoose;

const GroupSchema = new Schema({
  name: {
    type: String
  },
  code: {
    type: String
  },
  platform: {
    name: String, 
    code: String 
  },
  clients: [
    {
      wallet:{type:String},
      amount:{type: Number},
      type: {type: String, enum: ["percent", "fixed"]}
    }
  ],
  debtor: {
    wallet:{type:String},
    amount:{type: Number},
    type: {type: String, enum: ["percent", "fixed"]}
  }
});

module.exports = mongoose.model("Charge", GroupSchema);
