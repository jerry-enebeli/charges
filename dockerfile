FROM node:carbon

WORKDIR /eyowo/main

COPY . .

RUN npm install 

EXPOSE 50053

CMD [ "node", "index.js" ]