const grpc = require("grpc");
const rmd = require("randomstring");
const PRORTO_PATH = `${__dirname}/charge.proto`;

const EyowoCharge = grpc.load(PRORTO_PATH).EyowoCharge;

const client = new EyowoCharge.Charge(
  "35.232.82.180:80",
  grpc.credentials.createInsecure()
);

exports.CreateCharge = (req, res) => {
  const code = rmd.generate({
    length: 7,
    charset: "ABCDEFGHIJKLMN1234567890"
  });

  client.CreateCharge(
    {
      name: 'test',
      code: code,
      platform: 'mobile',
      clients: [
          {
            wallet: 'recipient',
            amount: 1,
            type: 'percent'
      }
    ],
      debtor: {
          wallet: 'sender',
          amount: 0,
          type: 'percent'
      }
    },
    (err, response) => {
     console.log(response)
    }
  );
};
